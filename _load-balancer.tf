resource "azurerm_lb" "lb-prod" {
    name = "lb-${var.company}-prod"
    resource_group_name = azurerm_resource_group.rg.name
    location = var.location
    sku = "Standard"

    frontend_ip_configuration {
        name = "pip-bcw-prod"
        public_ip_address_id = azurerm_public_ip.pip-prod.id
    }
}

resource "azurerm_public_ip" "pip-prod" {
    name = "pip-${var.company}-prod"
    resource_group_name = azurerm_resource_group.rg.name
    location = var.location
    allocation_method = "Static"
    sku = "Standard"
}

resource "azurerm_lb_backend_address_pool" "backend-pool-lb-prod" {
    name = "backend-pool-lb-${var.company}-prod"
    loadbalancer_id = azurerm_lb.lb-prod.id
}

resource "azurerm_lb_backend_address_pool_address" "backend-pool-address-lb-prod-1" {
    name = "backend-pool-address-lb-${var.company}-prod-1"
    backend_address_pool_id = azurerm_lb_backend_address_pool.backend-pool-lb-prod.id
    ip_address = azurerm_linux_virtual_machine.vm1-prod.private_ip_address
    virtual_network_id = azurerm_virtual_network.vnet.id
}

resource "azurerm_lb_backend_address_pool_address" "backend-pool-address-lb-prod-2" {
    name = "backend-pool-address-lb-${var.company}-prod-2"
    backend_address_pool_id = azurerm_lb_backend_address_pool.backend-pool-lb-prod.id
    ip_address = azurerm_linux_virtual_machine.vm2-prod.private_ip_address
    virtual_network_id = azurerm_virtual_network.vnet.id
}

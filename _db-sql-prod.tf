resource "azurerm_sql_database" "db-prod" {
        
    name = "db-${var.company}-prod-1"
    resource_group_name = azurerm_resource_group.rg.name
    location = var.location
    server_name = azurerm_sql_server.sql-srv-prod.name
    collation = "SQL_Latin1_General_CP1_CI_AS"
    create_mode = "Default"
    zone_redundant = false
    read_scale = false

    threat_detection_policy {
        state = "Disabled"
    }

    tags = {
        company = var.company
        environment = "prod"
        environment_index = 1
    }
    
}

resource "azurerm_sql_server" "sql-srv-prod" {
        
    name = "sql-srv-${var.company}-prod-1"
    resource_group_name = azurerm_resource_group.rg.name
    location = var.location
    version = "12.0"
    administrator_login = "bcw"
    administrator_login_password = "M0T-DE-P4SSE"
    
    tags = {
        company = var.company
        environment = "prod"
        environment_index = 1
    }

}
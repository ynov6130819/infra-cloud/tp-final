resource "azurerm_linux_virtual_machine" "vm1-prod" {
    name = "vm1-${var.company}-prod-1"
    resource_group_name = azurerm_resource_group.rg.name
    location = var.location
    size = "Standard_B1s"
    admin_username = "bcw-prod"
    network_interface_ids = [
        azurerm_network_interface.nic1-prod.id
    ]

    admin_password = random_password.password-prod-1.result
    disable_password_authentication = false

    os_disk {
        name = "osdisk1-${var.company}-prod-1"
        caching = "ReadWrite"
        storage_account_type = "Standard_LRS"
    }

    source_image_reference {
        publisher = "Canonical"
        offer = "UbuntuServer"
        sku = "18.04-LTS"
        version = "latest"
    }
}

resource "azurerm_linux_virtual_machine" "vm2-prod" {
    name = "vm2-${var.company}-prod-1"
    resource_group_name = azurerm_resource_group.rg.name
    location = var.location
    size = "Standard_B1s"
    admin_username = "bcw-prod"
    network_interface_ids = [
        azurerm_network_interface.nic2-prod.id
    ]

    admin_password = random_password.password-prod-2.result
    disable_password_authentication = false

    os_disk {
        name = "osdisk2-${var.company}-prod-1"
        caching = "ReadWrite"
        storage_account_type = "Standard_LRS"
    }

    source_image_reference {
        publisher = "Canonical"
        offer = "UbuntuServer"
        sku = "18.04-LTS"
        version = "latest"
    }
}

resource "azurerm_network_interface" "nic1-prod" {
    name = "nic1-${var.company}-prod-1"
    resource_group_name = azurerm_resource_group.rg.name
    location = var.location

    ip_configuration {
        name = "ipconfig1-${var.company}-prod-1"
        subnet_id = azurerm_subnet.sn-prod.id
        private_ip_address_allocation = "Dynamic"
    }
}

resource "azurerm_network_interface" "nic2-prod" {
    name = "nic2-${var.company}-prod-1"
    resource_group_name = azurerm_resource_group.rg.name
    location = var.location

    ip_configuration {
        name = "ipconfig2-${var.company}-prod-1"
        subnet_id = azurerm_subnet.sn-prod.id
        private_ip_address_allocation = "Dynamic"
    }
}

resource "random_password" "password-prod-1" {
    length           = 16
    special          = true
    override_special = "!#$%&*()-_=+[]{}<>:?"
}

resource "random_password" "password-prod-2" {
    length           = 16
    special          = true
    override_special = "!#$%&*()-_=+[]{}<>:?"
}
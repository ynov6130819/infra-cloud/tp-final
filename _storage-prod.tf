resource "azurerm_storage_account" "sa-prod" {
    name = "sa${var.company}prod1"
    resource_group_name = azurerm_resource_group.rg.name
    location = var.location
    account_tier = "Standard"
    account_replication_type = "LRS"

    tags = {
        company = var.company
        environment = "prod"
        environment_index = 1
    }
}

resource "azurerm_storage_account_network_rules" "sanr-prod" {
    default_action = "Allow"
    virtual_network_subnet_ids = [azurerm_subnet.sn-prod.id]
    storage_account_id = azurerm_storage_account.sa-prod.id
}

resource "azurerm_storage_container" "sc-prod" {
    name = "sc-${var.company}-prod-1"
    storage_account_name = azurerm_storage_account.sa-prod.name
    container_access_type = "private"
}

resource "azurerm_storage_blob" "sb-prod" {
    name = "sb-${var.company}-prod-1"
    storage_account_name = azurerm_storage_account.sa-prod.name
    storage_container_name = azurerm_storage_container.sc-prod.name
    type = "Block"
}
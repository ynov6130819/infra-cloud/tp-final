resource "azurerm_network_security_group" "nsg-admin" {
    name = "nsg-${var.company}-admin-1"
    resource_group_name = azurerm_resource_group.rg.name
    location = var.location
    
    security_rule {
        name = "allow-ssh"
        priority = 100
        direction = "Inbound"
        access = "Allow"
        protocol = "Tcp"
        source_port_range = "*"
        destination_port_range = "22"
        source_address_prefix = "*"
        destination_address_prefix = "*"
    }
    security_rule {
        name = "deny-http"
        priority = 101
        direction = "Inbound"
        access = "Deny"
        protocol = "Tcp"
        source_port_range = "*"
        destination_port_range = "80"
        source_address_prefix = "*"
        destination_address_prefix = "*"
    }
    security_rule {
        name = "deny-https"
        priority = 102
        direction = "Inbound"
        access = "Deny"
        protocol = "Tcp"
        source_port_range = "*"
        destination_port_range = "443"
        source_address_prefix = "*"
        destination_address_prefix = "*"
    }

    tags = {
        company = var.company
        environment = "admin"
        environment_index = 1
    }
}

resource "azurerm_subnet_network_security_group_association" "sn-nsg-admin" {
    subnet_id = azurerm_subnet.sn-admin.id
    network_security_group_id = azurerm_network_security_group.nsg-admin.id
}
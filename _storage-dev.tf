resource "azurerm_storage_account" "sa-dev" {
    name = "sa${var.company}dev1"
    resource_group_name = azurerm_resource_group.rg.name
    location = var.location
    account_tier = "Standard"
    account_replication_type = "LRS"

    tags = {
        company = var.company
        environment = "dev"
        environment_index = 1
    }
}

resource "azurerm_storage_account_network_rules" "sanr-dev" {
    default_action = "Allow"
    virtual_network_subnet_ids = [azurerm_subnet.sn-dev.id]
    storage_account_id = azurerm_storage_account.sa-dev.id
}

resource "azurerm_storage_container" "sc-dev" {
    name = "sc-${var.company}-dev-1"
    storage_account_name = azurerm_storage_account.sa-dev.name
    container_access_type = "private"
}

resource "azurerm_storage_blob" "sb-dev" {
    name = "sb-${var.company}-dev-1"
    storage_account_name = azurerm_storage_account.sa-dev.name
    storage_container_name = azurerm_storage_container.sc-dev.name
    type = "Block"
}
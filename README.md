﻿# DAT - BCW - Dominik TANKO


# Table des matières
[Préambule	1](#_toc134714552)

[Présentation et contexte	1](#_toc134714553)

[Architecture technique	2](#_toc134714554)

[L’environnent	2](#_toc134714555)

[Description	2](#_toc134714556)

[Caractéristiques de l’infrastructure	3](#_toc134714557)

[Adressage réseau	3](#_toc134714558)

[Spécifications machines virtuelles	4](#_toc134714559)

[Spécifications disques	4](#_toc134714560)

[Sécurité	4](#_toc134714561)

[Règle de sécurité	4](#_toc134714562)



# <a name="_toc134714552"></a>Préambule
Le présent Document d'Architecture Technique (DAT) décrit l'infrastructure Cloud Azure proposée pour répondre aux besoins de l'entreprise BCW en termes d'hébergement de sites Web, de bases de données et de stockage d'objets. Ce document est destiné aux équipes d'experts en infrastructure Cloud Azure de l'entreprise BCW pour la mise en place et l'exploitation de l'infrastructure décrite dans ce document.

# <a name="_toc134714553"></a>Présentation et contexte
BCW souhaite passer leur infrastructure sur le Cloud Azure, mais avec des critères à respecter :

Un sous-réseau de **production** avec :

- 3 machines virtuelles pour héberger un site web
- Un Load Balancer pour l’accès au site web
- Une base de données pour le site web
- Un stockage objet pour le site web

Un sous-réseau de **développement** avec :

- 1 machines virtuelles pour héberger un site web
- Une base de données pour le site web
- Un stockage objet pour le site web

Un sous-réseau d’**administration** avec :

- Une machine virtuelle avec une IP publique (Bastion)
# <a name="_toc134714554"></a>Architecture technique
## L’environnent 
![](diagram_architecture.png)<a name="_toc134714555"></a>

## <a name="_toc134714556"></a>Description
BCW va mettre en place la solution en utilisant les services Azure. L'environnement est déployé sur un tenant qui leur est dédié.

L'infrastructure proposée pour BCW comprendra les éléments suivants :

**IP NAT Gateway** : L'IP NAT Gateway est une adresse IP publique utilisée pour permettre aux machines virtuelles de communiquer avec Internet tout en masquant leurs adresses IP privées. Cette ressource est utilisée pour permettre la connectivité des machines virtuelles avec Internet tout en maintenant leur sécurité.

**NAT Gateway** : La ressource NAT Gateway est un service Azure qui permet de faire la traduction d'adresses réseau (NAT) pour les machines virtuelles hébergées dans un réseau virtuel Azure. Elle permet aux machines virtuelles d'accéder à Internet tout en protégeant leur adresse IP privée. Cette ressource est utilisée pour permettre la connectivité des machines virtuelles avec Internet tout en maintenant leur sécurité.

**IP VM Bastion** : L'IP VM Bastion est une adresse IP publique utilisée pour permettre l'accès à distance à la machine virtuelle Bastion. Cette ressource est utilisée pour permettre à l'équipe technique d'accéder à la machine virtuelle Bastion de manière sécurisée pour effectuer des tâches d'administration.

**VM Bastion** : La machine virtuelle Bastion est une machine virtuelle qui permet à l'équipe technique d'accéder à distance au réseau virtuel Azure de manière sécurisée. Elle fournit un point d'entrée sécurisé pour l'administration des machines virtuelles hébergeant le site web et de la base de données SQL. Cette ressource est utilisée pour permettre à l'équipe technique d'accéder au réseau virtuel Azure de manière sécurisée pour effectuer des tâches d'administration.

**IP Load Balancer** : L'IP Load Balancer est une adresse IP publique utilisée pour permettre l'accès au site web hébergé sur les machines virtuelles. Cette ressource est utilisée pour permettre aux utilisateurs d'accéder au site web de manière transparente, sans connaître les adresses IP des machines virtuelles qui hébergent le site web.

**VM 1-prod et VM 2-prod** : Les machines virtuelles 1 et 2 sont des machines virtuelles qui hébergent le site web de l'entreprise BCW. Elles sont configurées en mode redondant, ce qui signifie que le site web est disponible même en cas de panne d'une des machines virtuelles. Ces ressources sont utilisées pour héberger le site web de l'entreprise BCW.

**Base de données SQL** : La base de données SQL est une base de données relationnelle utilisée pour stocker les informations relatives aux utilisateurs du site web de l'entreprise BCW. Elle est configurée pour assurer la disponibilité et la sécurité des données stockées. Cette ressource est utilisée pour stocker les informations relatives aux utilisateurs du site web de l'entreprise BCW.
Ici nous avons deux bases de données, une pour l’environnement de développement at l’autre pour l’environnement de production.

**Compte de stockage** : Le compte de stockage est une ressource Azure utilisée pour stocker les fichiers statiques du site web de l'entreprise BCW, tels que les images et les fichiers CSS. Cette ressource est utilisée pour stocker les fichiers statiques du site web de l'entreprise BCW.
Ici nous avons deux comptes de stockages, un pour l’environnement de développement at l’autre pour l’environnement de production.
# <a name="_toc134714557"></a>Caractéristiques de l’infrastructure
## <a name="_toc134714558"></a>Adressage réseau
Pour cette infrastructure, voici la plage d’adresse IP privé que nous avons décidé de lui attribuer.

|**Adressage IP**|**Environnement**|
| :-: | :-: |
|192\.168.0.0/24|Développement|
|192\.168.1.0/24|Production|
|192\.168.2.0/24|Administration|

## <a name="_toc134714559"></a>Spécifications machines virtuelles

|**Nom**|**VCPU**|**RAM**|**Flavor**|**OS**|
| :-: | :-: | :-: | :-: | :-: |
|**vm-bastion-bcw-admin-1**|1|1 GB|Standard\_B1S|Ubuntu 20.04|
|**vm1-dev**|1|1 GB|Standard\_B1S|Ubuntu 20.04|
|**vm1-bcw-prod-1**|1|1 GB|Standard\_B1S|Ubuntu 20.04|
|**vm2-bcw-prod-1**|1|1 GB|Standard\_B1S|Ubuntu 20.04|

## <a name="_toc134714560"></a>Spécifications disques

|**Nom**|**Taille**|**Type**|**Nom VM**|
| :-: | :-: | :-: | :-: |
|**osdisk-bastion-bcw-admin-1**|30 Gio|SSD Premium LRS|vm-bastion-bcw-admin-1|
|**osdisk1-bcw-dev-1**|30 Gio|SSD Premium LRS|vm1-dev|
|**osdisk1-bcw-prod-1**|30 Gio|SSD Premium LRS|vm1-bcw-prod-1|
|**osdisk2-bcw-prod-1**|30 Gio|SSD Premium LRS|vm2-bcw-prod-1|

# <a name="_toc134714561"></a>Sécurité
## <a name="_toc134714562"></a>Règle de sécurité
Les règles de sécurité appliquées à l'infrastructure sont les suivantes :

- Les ports 80 et 443 seront ouverts sur le Load Balancer pour permettre l'accès au site web.
- Le port 22 (SSH) sera ouvert sur la machine virtuelle Bastion pour permettre l'accès administrateur au réseau.
4


variable "location" {
    description = "The location of the resource group"
    type = string
}

variable "company" {
    description = "The name of the company"
    type = string
}
resource "azurerm_linux_virtual_machine" "vm-bastion-admin" {
        
    name = "vm-bastion-${var.company}-admin-1"
    resource_group_name = azurerm_resource_group.rg.name
    location = var.location
    size = "Standard_B1s"
    admin_username = "bcw-admin"
    network_interface_ids = [
        azurerm_network_interface.nic-bastion-admin.id
    ]

    admin_password = random_password.password-admin.result
    disable_password_authentication = false

    os_disk {
        name = "osdisk-bastion-${var.company}-admin-1"
        caching = "ReadWrite"
        storage_account_type = "Standard_LRS"
    }

    source_image_reference {
        publisher = "Canonical"
        offer = "UbuntuServer"
        sku = "18.04-LTS"
        version = "latest"
    }
    
}

resource "azurerm_public_ip" "pip-bastion-admin" {
            
    name = "pip-bastion-${var.company}-admin-1"
    resource_group_name = azurerm_resource_group.rg.name
    location = var.location
    allocation_method = "Dynamic"

}

resource "azurerm_network_interface" "nic-bastion-admin" {
        
    name = "nic-bastion-${var.company}-admin-1"
    resource_group_name = azurerm_resource_group.rg.name
    location = var.location

    ip_configuration {
        name = "ipconfig-bastion-${var.company}-admin-1"
        subnet_id = azurerm_subnet.sn-admin.id
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id = azurerm_public_ip.pip-bastion-admin.id
    }

}

resource "random_password" "password-admin" {
    length           = 16
    special          = true
    override_special = "!#$%&*()-_=+[]{}<>:?"
}
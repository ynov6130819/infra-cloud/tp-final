resource "azurerm_virtual_network" "vnet" {

    name = "vnet-bcw"
    resource_group_name = azurerm_resource_group.rg.name
    location = var.location
    address_space = ["192.168.0.0/24", "192.168.1.0/24", "192.168.2.0/24"]
  
}

resource "azurerm_public_ip" "pip-nat" {

    name = "pip-bcw-nat"
    resource_group_name = azurerm_resource_group.rg.name
    location = var.location
    allocation_method = "Dynamic"

}

resource "azurerm_nat_gateway" "nat" {
    
    name = "nat-bcw"
    resource_group_name = azurerm_resource_group.rg.name
    location = var.location
    
}
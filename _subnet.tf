resource "azurerm_subnet" "sn-dev" {
    name = "sn-${var.company}-dev-1"
    resource_group_name = azurerm_resource_group.rg.name
    virtual_network_name = azurerm_virtual_network.vnet.name
    address_prefixes = ["192.168.0.0/24"]
    service_endpoints = ["Microsoft.Storage"]
}

resource "azurerm_subnet" "sn-prod" {
    name = "sn-${var.company}-prod-1"
    resource_group_name = azurerm_resource_group.rg.name
    virtual_network_name = azurerm_virtual_network.vnet.name
    address_prefixes = ["192.168.1.0/24"]
    service_endpoints = ["Microsoft.Storage"]
}

resource "azurerm_subnet" "sn-admin" {
    name = "sn-${var.company}-admin-1"
    resource_group_name = azurerm_resource_group.rg.name
    virtual_network_name = azurerm_virtual_network.vnet.name
    address_prefixes = ["192.168.2.0/24"]
}
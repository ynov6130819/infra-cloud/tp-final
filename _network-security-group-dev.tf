resource "azurerm_network_security_group" "nsg-dev" {
    name = "nsg-${var.company}-dev-1"
    resource_group_name = azurerm_resource_group.rg.name
    location = var.location
    
    security_rule {
        name = "allow-ssh"
        priority = 100
        direction = "Inbound"
        access = "Allow"
        protocol = "Tcp"
        source_port_range = "*"
        destination_port_range = "22"
        source_address_prefix = azurerm_linux_virtual_machine.vm-bastion-admin.private_ip_address
        destination_address_prefix = "*"
    }
    security_rule {
        name = "allow-http"
        priority = 101
        direction = "Inbound"
        access = "Allow"
        protocol = "Tcp"
        source_port_range = "*"
        destination_port_range = "80"
        source_address_prefix = "*"
        destination_address_prefix = "*"
    }
    security_rule {
        name = "allow-https"
        priority = 102
        direction = "Inbound"
        access = "Allow"
        protocol = "Tcp"
        source_port_range = "*"
        destination_port_range = "443"
        source_address_prefix = "*"
        destination_address_prefix = "*"
    }

    tags = {
        company = var.company
        environment = "dev"
        environment_index = 1
    }
}

resource "azurerm_subnet_network_security_group_association" "sn-nsg-dev" {
    subnet_id = azurerm_subnet.sn-dev.id
    network_security_group_id = azurerm_network_security_group.nsg-dev.id
}
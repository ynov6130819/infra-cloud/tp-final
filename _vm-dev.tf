resource "azurerm_linux_virtual_machine" "vm1-dev" {
    name = "vm1-${var.company}-dev-1"
    resource_group_name = azurerm_resource_group.rg.name
    location = var.location
    size = "Standard_B1s"
    admin_username = "bcw-dev"
    network_interface_ids = [
        azurerm_network_interface.nic1-dev.id
    ]

    admin_password = random_password.password-dev.result
    disable_password_authentication = false

    os_disk {
        name = "osdisk1-${var.company}-dev-1"
        caching = "ReadWrite"
        storage_account_type = "Standard_LRS"
    }

    source_image_reference {
        publisher = "Canonical"
        offer = "UbuntuServer"
        sku = "18.04-LTS"
        version = "latest"
    }
}

resource "azurerm_network_interface" "nic1-dev" {
    name = "nic1-${var.company}-dev-1"
    resource_group_name = azurerm_resource_group.rg.name
    location = var.location

    ip_configuration {
        name = "ipconfig1-${var.company}-dev-1"
        subnet_id = azurerm_subnet.sn-dev.id
        private_ip_address_allocation = "Dynamic"
    }
}

resource "random_password" "password-dev" {
    length           = 16
    special          = true
    override_special = "!#$%&*()-_=+[]{}<>:?"
}